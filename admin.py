import webapp2
import jinja2
from google.appengine.ext import ndb

from models import Paste



class FixPastes(webapp2.RequestHandler):
    def get(self):
        pastes = Paste.query().fetch()
        for p in pastes:
            p.is_private = False
            p.title = 'Untitled'
            p.put()
        return self.response.out.write('Updated %d pastes.' % len(pastes))


app = webapp2.WSGIApplication([
    ('/admin/fix_pastes/?', FixPastes),
], debug=True)
