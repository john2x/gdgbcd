import os

import webapp2
import jinja2
from google.appengine.api import users, taskqueue
from google.appengine.ext import ndb

from models import Paste

jinja = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__),
                                                'templates'))
)

class BaseHandler(webapp2.RequestHandler):
    def __init__(self, request, response):
        self.initialize(request, response)


class CreatePaste(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        context = {
            'user': user,
            'logout_url': users.create_logout_url('/'),
            'login_url': users.create_login_url(),
        }
        template = jinja.get_template('create_paste.html')
        return self.response.out.write(template.render(context))

    def post(self):
        user = users.get_current_user()
        content = self.request.get('content')
        title = self.request.get('title')
        is_private = bool(self.request.get('is_private'))
        as_anonymous = bool(self.request.get('as_anonymous'))
        lifespan = int(self.request.get('lifespan', 3600*24*7))
        if as_anonymous:
            user = None
        p = Paste.new(title, content, is_private=is_private, owner=user)
        if lifespan > 0:
            taskqueue.add(
                method='post',
                url='/background/delete/' + str(p.key.id()),
                countdown=lifespan
            )
        return self.redirect('/p/' + str(p.key.id()))


class ViewPaste(webapp2.RequestHandler):
    def get(self, id):
        user = users.get_current_user()
        key = ndb.Key('Paste', int(id))
        p = key.get()
        if not p:
            return self.abort(404)
        template = jinja.get_template('view_paste.html')
        context = {
            'user': user,
            'logout_url': users.create_logout_url('/'),
            'login_url': users.create_login_url(),
            'paste': p
        }
        return self.response.out.write(template.render(context))


class PublicPastes(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        pastes = Paste.all_public().fetch()
        template = jinja.get_template('list_pastes.html')
        context = {
            'user': user,
            'logout_url': users.create_logout_url('/'),
            'login_url': users.create_login_url(),
            'pastes': pastes,
        }
        return self.response.out.write(template.render(context))


class MyPastes(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        pastes = Paste.owned_by(user).fetch()
        template = jinja.get_template('list_pastes.html')
        context = {
            'user': user,
            'logout_url': users.create_logout_url('/'),
            'login_url': users.create_login_url(),
            'pastes': pastes,
        }
        return self.response.out.write(template.render(context))


class EditPaste(webapp2.RequestHandler):
    def get(self, id):
        user = users.get_current_user()
        key = ndb.Key('Paste', int(id))
        p = key.get()
        if not p or p.owner != user:
            return self.abort(404)
        template = jinja.get_template('edit_paste.html')
        context = {
            'user': user,
            'logout_url': users.create_logout_url('/'),
            'login_url': users.create_login_url(),
            'paste': p,
        }
        return self.response.out.write(template.render(context))

    def post(self, id):
        user = users.get_current_user()
        key = ndb.Key('Paste', int(id))
        p = key.get()
        if not p or p.owner != user:
            return self.abort(404)
        content = self.request.get('content')
        title = self.request.get('title')
        is_private = bool(self.request.get('is_private'))
        p.title = title
        p.set_content(content)
        p.is_private = is_private
        p.put()
        return self.redirect('/p/' + str(p.key.id()))


class DeletePaste(webapp2.RequestHandler):
    def post(self, id):
        user = users.get_current_user()
        key = ndb.Key('Paste', int(id))
        p = key.get()
        if not p or p.owner != user:
            return self.abort(404)
        key.delete()
        return self.redirect('/mine')


app = webapp2.WSGIApplication([
    ('/?', PublicPastes),
    ('/public/?', PublicPastes),
    ('/paste/?', CreatePaste),
    ('/p/(.*)/?', ViewPaste),
    ('/mine/?', MyPastes),
    ('/m/(.*)/delete/?', DeletePaste),
    ('/m/(.*)/?', EditPaste),
], debug=True)
