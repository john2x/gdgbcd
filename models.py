from google.appengine.ext import ndb

class Paste(ndb.Model):
    owner = ndb.UserProperty()
    title = ndb.StringProperty(default='Untitled')
    content = ndb.BlobProperty()
    is_private = ndb.BooleanProperty(default=False)
    created = ndb.DateTimeProperty(auto_now_add=True)

    @classmethod
    def new(cls, title, content, is_private=False, owner=None, encoding='utf8'):
        p = cls(title=title, is_private=is_private, owner=owner)
        p.set_content(content)
        p.put()
        return p

    def set_content(self, content, encoding='utf8'):
        content = content.encode(encoding)
        self.content = content

    @property
    def decoded_content(self):
        return self.content.decode('utf8')

    @classmethod
    def all_public(cls):
        return cls.query(cls.is_private == False).order(-cls.created)

    @classmethod
    def owned_by(cls, user):
        return cls.query(cls.owner == user).order(-cls.created)

    @property
    def pretty_created(self):
        return self.created.strftime('%b %d, %Y %I:%M:%S %p')
