import webapp2

from google.appengine.ext import ndb
from models import Paste

class DeletePaste(webapp2.RequestHandler):
    def post(self, id):
        key = ndb.Key('Paste', int(id))
        paste = key.get()
        if not paste:
            return
        key.delete()


app = webapp2.WSGIApplication([
    ('/background/delete/(.*)/?', DeletePaste),
], debug=True)
